<?php

use Illuminate\Database\Seeder;
use App\Member;
class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $member_list=[
    		['nrp' => '5115100001','nama' => 'Damai Marisa B'] ,
    		['nrp' => '5115100066','nama' => 'Satria Aryawan'] ,
    		['nrp' => '5115100094','nama' => 'Achmadaniar Anindya Rhosady'] ,
    		['nrp' => '5115100135','nama' => 'Anisa Putri D'] ,
    		['nrp' => '5115100172','nama' => 'Joshua Resamuel'] ,
    	];
        foreach ($member_list as $member ) {
        	Member::create($member);
        }
    }
}
