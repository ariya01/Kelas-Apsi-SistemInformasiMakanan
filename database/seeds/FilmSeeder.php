<?php

use Illuminate\Database\Seeder;
use App\Film;
class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $film_list=[
    		['nama_film' => 'Logan','tahun' => '2017'] ,
    		['nama_film' => 'Jurassic World','tahun' => '2015'] ,
    		['nama_film' => 'The Expendables 3','tahun' => '2014'] ,
    		['nama_film' => 'The Fate and Furious','tahun' => '2017'] ,
    		['nama_film' => 'Deadpool','tahun' => '2016'] ,
    		['nama_film' => 'Guardian of The Galaxy','tahun' => '2014'] ,
    		['nama_film' => 'The Avengers','tahun' => '2012'] ,
    		['nama_film' => 'Titanic','tahun' => '1997'] ,
    		
    	];
        foreach ($film_list as $film ) {
        	Film::create($film);
        }
    }
}
