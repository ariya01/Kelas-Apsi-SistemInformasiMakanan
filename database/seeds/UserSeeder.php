<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_list=[
    		['name' => 'Damai Marisa B','username' => '5115100001','password' => bcrypt('5115100001')] ,
    		['name' => 'Satria Aryawan','username' => '5115100066','password' => bcrypt('5115100066')] ,
    		['name' => 'Achmadaniar Anindya Rhosady','username' => '5115100094','password' => bcrypt('5115100094')] ,
    		['name' => 'Anisa Putri D','username' => '5115100135','password' => bcrypt('5115100135')] ,
    		['name' => 'Joshua Resamuel','username' => '5115100172','password' => bcrypt('5115100172')] ,
    	];
        foreach ($user_list as $user ) {
        	User::create($user);
        }
    }
}
