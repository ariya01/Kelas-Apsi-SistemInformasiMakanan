@extends('layouts.master')
@section('title','Makan.in | Menu Makanan')
@section('additional_css')
   
@endsection
@section('content')
    <h2>Menu Makanan</h2><br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 pull-right">
        	<ul class="list-inline links-list pull-right">
                <li> <a class="btn btn-success" href="#baru" data-toggle="modal"> <i class="entypo-plus left"></i>Menu Baru </a> </li>
            </ul>
        </div>
    </div><br/>
    @include('layouts.flash_msg')
    <div>
    	<table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Nama Menu Makanan</th>
                    <th>Jumlah Tersedia</th>
                    <th style="width: 45%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $datas)
            	<tr>
            		<td>{{$datas->nama_menu}}</td>
            		<td>{{$datas->jumlah}} buah</td>
            		<td>
                        <button class="btn btn-default btn-sm btn-icon icon-left edit-modal" value="{{$datas->id}}" > <i class="entypo-pencil"></i> Edit
                        </button>
                        <a href="{{route('menu_hapus',$datas->id)}}" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i> Hapus
                        </a>
                        <button class="btn btn-primary btn-sm btn-icon icon-left tambah-modal" value="{{$datas->id}}" > <i class="entypo-plus"></i> Tambah
                        </button>
                        <button class="btn btn-orange btn-sm btn-icon icon-left kurang-modal" value="{{$datas->id}}" > <i class="entypo-minus"></i> Kurang
                        </button>
                        <button class="btn btn-blue btn-sm btn-icon icon-left detail-modal" value="{{$datas->id}}" > <i class="entypo-info"></i> Detail
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><br/>
    <!-- Modals -->
    <div class="modal fade" id="baru">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Menu Baru</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('menu_baru')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="nama" class="col-md-5 control-label">Nama Menu</label>
                                    <div class="col-md-7">
                                        <input id="nama" type="text" class="form-control" name="nama" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kategori" class="col-md-5 control-label">Kategori Menu</label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="kategori" name="kategori">
                                            <option value="makanan">Makanan</option>
                                            <option value="minuman">Minuman</option>
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah" type="number" class="form-control" name="jumlah" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="harga" class="col-md-5 control-label">Harga</label>
                                    <div class="col-md-7">
                                        <input id="harga" type="number" class="form-control" name="harga" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi" class="col-md-5 control-label">Deskripsi</label>
                                    <div class="col-md-7">
                                        <textarea id="deskripsi" class="form-control" name="deskripsi" rows="5" required></textarea>
                                    </div>
                                </div>
                                <label for="image" class="col-md-5 control-label">Gambar</label>
                                    <div class="col-md-7">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput"> <img src="http://placehold.it/200x150" alt="..."> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                            <div> <span class="btn btn-white btn-file"> <span class="fileinput-new">Select image</span> <span class="fileinput-exists">Change</span> <input type="file" name="image" accept="image/*"> </span> <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Tambah Menu Baru
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Menu</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('menu_edit')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input id="id_edit" type="hidden" class="form-control" name="id_edit" required>
                                <div class="form-group">
                                    <label for="nama_edit" class="col-md-5 control-label">Nama Menu</label>
                                    <div class="col-md-7">
                                        <input id="nama_edit" type="text" class="form-control" name="nama_edit" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kategori_edit" class="col-md-5 control-label">Kategori Menu</label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="kategori_edit" name="kategori_edit">
                                            <option value="makanan">Makanan</option>
                                            <option value="minuman">Minuman</option>
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_edit" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_edit" type="number" class="form-control" name="jumlah_edit" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="harga_edit" class="col-md-5 control-label">Harga</label>
                                    <div class="col-md-7">
                                        <input id="harga_edit" type="number" class="form-control" name="harga_edit" required>
                                    </div>
                                </div>
                               	<div class="form-group">
                                    <label for="deskripsi_edit" class="col-md-5 control-label">Deskripsi</label>
                                    <div class="col-md-7">
                                        <textarea id="deskripsi_edit" class="form-control" name="deskripsi_edit" rows="5" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Edit Menu
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tambah">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Menu</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('menu_tambah')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input id="id_tambah" type="hidden" class="form-control" name="id_tambah" required>
                                <div class="form-group">
                                    <label for="nama_tambah" class="col-md-5 control-label">Nama Menu</label>
                                    <div class="col-md-7">
                                        <input id="nama_tambah" type="text" class="form-control" name="nama_tambah" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_tambah" class="col-md-5 control-label">Jumlah Sekarang</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_tambah" type="text" class="form-control" name="jumlah_tambah" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tambahan" class="col-md-5 control-label">Tambahan</label>
                                    <div class="col-md-7">
                                        <input id="tambahan" type="number" class="form-control" name="tambahan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Tambah Menu
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="kurang">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Kurang Menu</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('menu_kurang')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input id="id_kurang" type="hidden" class="form-control" name="id_kurang" required>
                                <div class="form-group">
                                    <label for="nama_kurang" class="col-md-5 control-label">Nama Menu</label>
                                    <div class="col-md-7">
                                        <input id="nama_kurang" type="text" class="form-control" name="nama_kurang" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_kurang" class="col-md-5 control-label">Jumlah Sekarang</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_kurang" type="text" class="form-control" name="jumlah_kurang" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kurangan" class="col-md-5 control-label">Kurangan</label>
                                    <div class="col-md-7">
                                        <input id="kurangan" type="number" class="form-control" name="kurangan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Kurang Menu
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Menu</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="#" method="POST">
                                {{ csrf_field() }}
                                <input id="id_detail" type="hidden" class="form-control" name="id_detail" required>
                                <div class="form-group">
                                    <label for="nama_detail" class="col-md-5 control-label">Nama Menu</label>
                                    <div class="col-md-7">
                                        <input id="nama_detail" type="text" class="form-control" name="nama_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kategori_detail" class="col-md-5 control-label">Kategori Menu</label>
                                    <div class="col-md-7">
                                        <input id="kategori_detail" type="text" class="form-control" name="kategori_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_detail" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_detail" type="number" class="form-control" name="jumlah_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="harga_detail" class="col-md-5 control-label">Harga</label>
                                    <div class="col-md-7">
                                        <input id="harga_detail" type="text" class="form-control" name="harga_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi_detail" class="col-md-5 control-label">Deskripsi</label>
                                    <div class="col-md-7">
                                        <textarea id="deskripsi_detail" class="form-control" name="deskripsi_detail" rows="5" required readonly></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="image_detail" class="col-md-5 control-label">Gambar</label>
                                    <div class="col-md-7">
                                        <img id="image_detail" class="img-rounded" alt="Gambar" width="100%">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_js')
	<script src="{{asset('assets/js/fileinput.js')}}" id="script-resource-8"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var url = "menu/find";
            $('.edit-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_edit').val(data.id);
                    $('#nama_edit').val(data.nama_menu);
                    $('#jumlah_edit').val(data.jumlah);
                    $('#harga_edit').val(data.harga);
                    $('#deskripsi_edit').val(data.deskripsi);
                    $('#edit').modal('show');
                }) 
            });
            $('.tambah-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_tambah').val(data.id);
                    $('#nama_tambah').val(data.nama_menu);
                    $('#jumlah_tambah').val(data.jumlah+' buah');
                    $('#tambah').modal('show');
                }) 
            });
            $('.kurang-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_kurang').val(data.id);
                    $('#nama_kurang').val(data.nama_menu);
                    $('#jumlah_kurang').val(data.jumlah+' buah');
                    $('#kurang').modal('show');
                }) 
            });
            $('.detail-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_detail').val(data.id);
                    $('#nama_detail').val(data.nama_menu);
                    $('#kategori_detail').val(data.kategori);
                    $('#jumlah_detail').val(data.jumlah);
                    $('#harga_detail').val(data.harga);
                    $('#deskripsi_detail').val(data.deskripsi);
                    $('#image_detail').attr('src','images/'+data.image);
                    $('#detail').modal('show');
                }) 
            });
        });
    </script>
@endsection