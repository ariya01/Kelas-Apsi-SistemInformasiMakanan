<div class="sidebar-menu fixed">
    <div class="sidebar-menu-inner">
        <header class="logo-env">
            <!-- logo -->
            <div class="logo">
                <a href="{{route('index')}}"> <img src="{{asset('assets/images/logo@2x.png')}}" width="120" alt="" /> </a>
            </div>
            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon with-animation">
                    <i class="entypo-menu"></i> </a>
            </div>
            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation">
                    <i class="entypo-menu"></i> </a>
            </div>
        </header>
        <ul id="main-menu" class="main-menu">
            <li> <a href="{{route('dashboard')}}"><i class="entypo-gauge"></i><span class="title">Dashboard</span></a> </li>
            <li> <a href="{{route('stok')}}"><i class="entypo-retweet"></i><span class="title">Stok Bahan</span></a> </li>
            <li> <a href="{{route('menu')}}"><i class="entypo-tools"></i><span class="title">Menu Makanan</span></a> </li>
            <li> <a href="{{route('pembayaran')}}"><i class="entypo-basket"></i><span class="title">Pembayaran</span></a> </li>
            <li> <a href="{{route('antrian')}}"><i class="entypo-users"></i><span class="title">Antrian</span></a> </li>
        </ul>
    </div>
</div>