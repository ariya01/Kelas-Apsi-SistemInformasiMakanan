@extends('layouts.master')
@section('title','Makan.in | Antrian')
@section('additional_css')
   
@endsection
@section('content')
    <h2>Antrian</h2><br>
    @include('layouts.flash_msg')
    <div>
    	<table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Antrian</th>
                    <th>ID Pembayaran</th>
                    <th>Status</th>
                    <th style="width: 45%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $datas)
            	<tr>
            		<td>{{$datas->id}}</td>
            		<td>{{$datas->pembayaran_id}}</td>
            		<td>{{$datas->status}}</td>
            		<td>
                        <button class="btn btn-default btn-sm btn-icon icon-left edit-modal" value="{{$datas->id}}" > <i class="entypo-pencil"></i> Edit
                        </button>
                        <a href="{{route('antrian_hapus',$datas->id)}}" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i> Hapus
                        </a>
                        <button class="btn btn-blue btn-sm btn-icon icon-left detail-modal" value="{{$datas->id}}" > <i class="entypo-info"></i> Detail
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><br/>
    <!-- Modals -->
    <div class="modal fade" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Antrian</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                        	<h4 id="id_pembayaran">ID Pembayaran: 3</h4>
                        	<table class="table table-bordered responsive" >
					            <thead>
					                <tr>
					                    <th>Nama Menu</th>
					                    <th>Quantity</th>
					                    <th>Harga</th>
					                    <th>Total</th>
					                </tr>
					            </thead>
					            <tbody id="detail_list">

					            </tbody>
					            <tfoot>
					            	<tr>
					            		<td colspan="3" style="vertical-align:middle;text-align: right"><strong>Total Keseluruhan</strong></td>
					            		<td id="detail_all"></td>
					            	</tr>
					            </tfoot>
					        </table>
                            <form class="form-horizontal" action="{{route('antrian_edit')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input id="id_edit" type="hidden" class="form-control" name="id_edit" required>
                                <div class="form-group">
                                    <label for="status_edit" class="col-md-5 control-label">Status Antrian</label>
                                    <div class="col-md-7">
                                        <select required class="form-control" id="status" name="status">
				            				<option>
                                                <option value="Proses" selected>Proses</option>
                                                <option value="Selesai">Selesai</option>
                                                <option value="Gagal">Gagal</option>
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Edit Antrian
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <div class="modal fade" id="detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Antrian</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <table class="table table-bordered responsive" >
					            <thead>
					                <tr>
					                    <th>Nama Menu</th>
					                    <th>Quantity</th>
					                    <th>Harga</th>
					                    <th>Total</th>
					                </tr>
					            </thead>
					            <tbody id="detail_list2">

					            </tbody>
					            <tfoot>
					            	<tr>
					            		<td colspan="3" style="vertical-align:middle;text-align: right"><strong>Total Keseluruhan</strong></td>
					            		<td id="detail_all2"></td>
					            	</tr>
					            </tfoot>
					        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_js')
	<script src="{{asset('assets/js/fileinput.js')}}" id="script-resource-8"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var url = "antrian/find";
            var url2 = "pembayaran/find";
            $('.edit-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data

                    console.log(data);
                    var pembayaran_id=data.pembayaran_id;
                    $('#id_pembayaran').html('ID Pembayaran: '+pembayaran_id);
                    $.get(url2 + '/' + pembayaran_id, function (data2) {
	                    //success data
	                   $( "#detail_list" ).html(''); 
	                   for(i=0;i<(data2.menu).length;i++)
	                   {
		                   $( "#detail_list" ).append( 
				        	"<tr><td>"+data2.menu[i]+"</td><td>"+data2.jumlah[i]+"</td><td>"+data2.harga[i]+"</td><td>"+(data2.jumlah[i]*data2.harga[i])+"</td></tr>"
				        	);
	                   }
	                   $('#detail_all').html(data2.total);
	                }) 
	                $('#id_edit').val(data.id);

	                $('#edit').modal('show');
                }) 
            });
            $('.detail-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data

                    console.log(data);
                    var pembayaran_id=data.pembayaran_id;
                    $.get(url2 + '/' + pembayaran_id, function (data2) {
	                    //success data
	                   $( "#detail_list" ).html(''); 
	                   for(i=0;i<(data2.menu).length;i++)
	                   {
		                   $( "#detail_list2" ).append( 
				        	"<tr><td>"+data2.menu[i]+"</td><td>"+data2.jumlah[i]+"</td><td>"+data2.harga[i]+"</td><td>"+(data2.jumlah[i]*data2.harga[i])+"</td></tr>"
				        	);
	                   }
	                   $('#detail_all2').html(data2.total);
	                }) 

	                $('#detail').modal('show');
                }) 
            });
        });
    </script>
@endsection