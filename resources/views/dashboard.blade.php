@extends('layouts.master')
@section('title','Makan.in | Dashboard')
@section('additional_css')
    <!-- insert your additional css here -->
@endsection
@section('content')
    <h2>Dashboard</h2><br>
    @include('layouts.flash_msg')
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <div class="tile-stats tile-green">
                <div class="icon"><i class="entypo-retweet"></i></div>
                <div class="num" data-start="0" data-end="{{$stok}}" data-postfix="" data-duration="1500" data-delay="0">0</div>
                <h3>Stok Bahan</h3>
                <p>Jumlah Stok Bahan Makanan.</p>
            </div>
        </div>
        <div class="clear visible-xs"></div>
        <div class="col-sm-6 col-xs-12">
            <div class="tile-stats tile-aqua">
                <div class="icon"><i class="entypo-tools"></i></div>
                <div class="num" data-start="0" data-end="{{$menu}}" data-postfix="" data-duration="1500" data-delay="600">0</div>
                <h3>Menu Makanan</h3>
                <p>Jumlah Menu Makanan Yang tersedia.</p>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="tile-stats tile-blue">
                <div class="icon"><i class="entypo-basket"></i></div>
                <div class="num" data-start="0" data-end="{{$pembayaran}}" data-postfix="" data-duration="1500" data-delay="1200">0</div>
                <h3>Pembayaran</h3>
                <p>Jumkah transaksi pembayaran.</p>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="tile-stats tile-red">
                <div class="icon"><i class="entypo-users"></i></div>
                <div class="num" data-start="0" data-end="{{$antrian}}" data-postfix="" data-duration="1500" data-delay="1800">0</div>
                <h3>Antrian</h3>
                <p>Jumlah Antrian.</p>
            </div>
        </div>
    </div><br/>
@endsection
@section('additional_js')
    <!-- insert your additional js here -->
@endsection