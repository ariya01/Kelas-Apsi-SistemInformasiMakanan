@extends('layouts.master')
@section('title','Makan.in | Stok bahan')
@section('additional_css')
   
@endsection
@section('content')
    <h2>Stok Bahan</h2><br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 pull-right">
        	<ul class="list-inline links-list pull-right">
                <li> <a class="btn btn-success" href="#baru" data-toggle="modal"> <i class="entypo-plus left"></i>Stok Baru </a> </li>
            </ul>
        </div>
    </div><br/>
    @include('layouts.flash_msg')
    <div>
    	<table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Nama Stok bahan</th>
                    <th>Jumlah Stok/Satuan</th>
                    <th style="width: 45%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $datas)
            	<tr>
            		<td>{{$datas->nama_stok}}</td>
            		<td>{{$datas->jumlah}} {{$datas->satuan}}</td>
            		<td>
                        <button class="btn btn-default btn-sm btn-icon icon-left edit-modal" value="{{$datas->id}}" > <i class="entypo-pencil"></i> Edit
                        </button>
                        <a href="{{route('stok_hapus',$datas->id)}}" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i> Hapus
                        </a>
                        <button class="btn btn-primary btn-sm btn-icon icon-left tambah-modal" value="{{$datas->id}}" > <i class="entypo-plus"></i> Tambah
                        </button>
                        <button class="btn btn-orange btn-sm btn-icon icon-left kurang-modal" value="{{$datas->id}}" > <i class="entypo-minus"></i> Kurang
                        </button>
                        <button class="btn btn-blue btn-sm btn-icon icon-left detail-modal" value="{{$datas->id}}" > <i class="entypo-info"></i> Detail
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><br/>
    <!-- Modals -->
    <div class="modal fade" id="baru">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Stok Baru</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('stok_baru')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="nama" class="col-md-5 control-label">Nama Stok Bahan</label>
                                    <div class="col-md-7">
                                        <input id="nama" type="text" class="form-control" name="nama" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah" type="number" class="form-control" name="jumlah" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="satuan" class="col-md-5 control-label">Satuan</label>
                                    <div class="col-md-7">
                                        <input id="satuan" type="text" class="form-control" name="satuan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Tambah Stok Baru
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Stok</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('stok_edit')}}" method="POST">
                                {{ csrf_field() }}
                                <input id="id_edit" type="hidden" class="form-control" name="id_edit" required>
                                <div class="form-group">
                                    <label for="nama_edit" class="col-md-5 control-label">Nama Stok Bahan</label>
                                    <div class="col-md-7">
                                        <input id="nama_edit" type="text" class="form-control" name="nama_edit" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_edit" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_edit" type="number" class="form-control" name="jumlah_edit" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="satuan_edit" class="col-md-5 control-label">Satuan</label>
                                    <div class="col-md-7">
                                        <input id="satuan_edit" type="text" class="form-control" name="satuan_edit" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Edit Stok
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tambah">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Stok</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('stok_tambah')}}" method="POST">
                                {{ csrf_field() }}
                                <input id="id_tambah" type="hidden" class="form-control" name="id_tambah" required>
                                <div class="form-group">
                                    <label for="nama_tambah" class="col-md-5 control-label">Nama Stok Bahan</label>
                                    <div class="col-md-7">
                                        <input id="nama_tambah" type="text" class="form-control" name="nama_tambah" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_tambah" class="col-md-5 control-label">Jumlah Sekarang</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_tambah" type="text" class="form-control" name="jumlah_tambah" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tambahan" class="col-md-5 control-label">Tambahan</label>
                                    <div class="col-md-7">
                                        <input id="tambahan" type="number" class="form-control" name="tambahan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Tambah Stok
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="kurang">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Kurang Stok</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('stok_kurang')}}" method="POST">
                                {{ csrf_field() }}
                                <input id="id_kurang" type="hidden" class="form-control" name="id_kurang" required>
                                <div class="form-group">
                                    <label for="nama_kurang" class="col-md-5 control-label">Nama Stok Bahan</label>
                                    <div class="col-md-7">
                                        <input id="nama_kurang" type="text" class="form-control" name="nama_kurang" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_kurang" class="col-md-5 control-label">Jumlah Sekarang</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_kurang" type="text" class="form-control" name="jumlah_kurang" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kurangan" class="col-md-5 control-label">Kurangan</label>
                                    <div class="col-md-7">
                                        <input id="kurangan" type="number" class="form-control" name="kurangan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Kurang Stok
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Stok</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="#" method="POST">
                                {{ csrf_field() }}
                                <input id="id_detail" type="hidden" class="form-control" name="id_detail" required>
                                <div class="form-group">
                                    <label for="nama_detail" class="col-md-5 control-label">Nama Stok Bahan</label>
                                    <div class="col-md-7">
                                        <input id="nama_detail" type="text" class="form-control" name="nama_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_detail" class="col-md-5 control-label">Jumlah</label>
                                    <div class="col-md-7">
                                        <input id="jumlah_detail" type="number" class="form-control" name="jumlah_detail" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="satuan_detail" class="col-md-5 control-label">Satuan</label>
                                    <div class="col-md-7">
                                        <input id="satuan_detail" type="text" class="form-control" name="satuan_detail" required readonly>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_js')
    <script type="text/javascript">
        $(document).ready(function(){
            var url = "stok/find";
            $('.edit-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_edit').val(data.id);
                    $('#nama_edit').val(data.nama_stok);
                    $('#jumlah_edit').val(data.jumlah);
                    $('#satuan_edit').val(data.satuan);
                    $('#edit').modal('show');
                }) 
            });
            $('.tambah-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_tambah').val(data.id);
                    $('#nama_tambah').val(data.nama_stok);
                    $('#jumlah_tambah').val(data.jumlah+' '+data.satuan);
                    $('#tambah').modal('show');
                }) 
            });
            $('.kurang-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_kurang').val(data.id);
                    $('#nama_kurang').val(data.nama_stok);
                    $('#jumlah_kurang').val(data.jumlah+' '+data.satuan);
                    $('#kurang').modal('show');
                }) 
            });
            $('.detail-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    console.log(data);
                    $('#id_detail').val(data.id);
                    $('#nama_detail').val(data.nama_stok);
                    $('#jumlah_detail').val(data.jumlah);
                    $('#satuan_detail').val(data.satuan);
                    $('#detail').modal('show');
                }) 
            });
        });
    </script>
@endsection