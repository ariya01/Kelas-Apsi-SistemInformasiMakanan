<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Makan.in | Masuk</title>  
      <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
  <hgroup>
  <h1>Masuk</h1>
</hgroup>
<form method="POST" action="{{route('login')}}">
     {{ csrf_field() }}
  <div class="group">
    <input name="username" type="text"><span class="highlight"></span><span class="bar"></span>
    <label>Username</label>
  </div>
  <div class="group">
    <input name="password" type="password"><span class="highlight"></span><span class="bar"></span>
    <label>Password</label>
  </div>
  <button type="submit" class="button buttonBlue">Login
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>
<footer>
</footer>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="{{asset('js/index.js')}}"></script>

</body>
</html>
