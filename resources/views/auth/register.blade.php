<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Makan.in | Daftar</title>  
      <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
  <hgroup>
  <h1>Masuk</h1>
</hgroup>
<form method="POST" action="{{route('register')}}">
     {{ csrf_field() }}
    <div class="group">
    <input name="name" type="text"><span class="highlight"></span><span class="bar"></span>
    <label>Nama</label>
  </div>
  <div class="group">
    <input name="username" type="text"><span class="highlight"></span><span class="bar"></span>
    <label>Username</label>
  </div>
  <div class="group">
    <input name="password" type="password"><span class="highlight"></span><span class="bar"></span>
    <label>Password</label>
  </div>
  <div class="group">
    <input name="password_confirmation" type="password"><span class="highlight"></span><span class="bar"></span>
    <label>Confirm Password</label>
  </div>
  <button type="submit" class="button buttonBlue">Daftar
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>
<footer>
</footer>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="{{asset('js/index.js')}}"></script>

</body>
</html>
