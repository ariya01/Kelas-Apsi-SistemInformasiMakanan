@extends('layouts.master')
@section('title','Makan.in | Pembayaran')
@section('additional_css')
   <style type="text/css">
   	@media screen {
	    #printSection {
	        display: none;
	    }
	}
	@media print {
	    body * {
	        visibility:hidden;
	    }
	    #printSection, #printSection * {
	        visibility:visible;
	    }
	    #printSection {
	        position:absolute;
	        left:0;
	        top:0;
	    }
	}
   </style>
@endsection
@section('content')
    <h2>Pembayaran</h2><br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 pull-right">
        	<ul class="list-inline links-list pull-right">
                <li> <a class="btn btn-success" href="#baru" data-toggle="modal"> <i class="entypo-plus left"></i>Pembayaran</a> </li>
            </ul>
        </div>
    </div><br/>
    @include('layouts.flash_msg')
    <div>
    	<table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>ID Pembayaran</th>
                    <th>Total</th>
                    <th style="width: 30%">Actions</th>
                </tr>
            </thead>
            <tbody>
               @foreach($data as $datas)
            	<tr>
            		<td>{{$datas->id}}</td>
            		<td>Rp. {{$datas->total}}</td>
            		<td>
                        <a href="{{route('pembayaran_hapus',$datas->id)}}" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i> Hapus
                        </a>
                        <button class="btn btn-blue btn-sm btn-icon icon-left detail-modal" value="{{$datas->id}}" > <i class="entypo-info"></i> Detail
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><br/>
    <!-- Modals -->
    <div class="modal fade" id="baru">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Pembayaran Baru</h4> </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form class="form-horizontal" action="{{route('pembayaran_baru')}}" method="POST" enctype="multipart/form-data" >
                                {{ csrf_field() }}
                                <table class="table table-bordered responsive">
						            <thead>
						                <tr>
						                    <th>Nama Menu</th>
						                    <th>Quantity</th>
						                    <th>Harga</th>
						                    <th>Total</th>
						                </tr>
						            </thead>
						            <tbody id="baru_list">
						               <tr>
						            		<td>
						            			<select required onchange='set_harga(0,this)' class="form-control" id="menu" name="menu[0]">
						            				<option disabled selected value>-Pilih Menu-</option>
													@foreach($menu as $menus)
		                                                <option value="{{$menus->id}}">{{$menus->nama_menu}}</option>
		                                            @endforeach
												</select>
											</td>
						            		<td>
						            			<input id="jumlah0" type="number" class="form-control" name="jumlah[0]" placeholder="Jumlah" value="1" onchange='set_total(0)' required>
						            		</td>
						            		<td>
						            			<input id="harga0" type="number" class="form-control" name="harga[0]" placeholder="Harga" required readonly>
						            		</td>
						            		<td>
						            			<input id="total0" type="number" class="form-control" name="total[0]" placeholder="total" required readonly ">
						            		</td>
					            		</tr>
						            </tbody>
						            <tfoot>
						            	<tr>
						            		<td colspan="3" style="vertical-align:middle;text-align: right"><strong>Total Keseluruhan</strong></td>
						            		<td><input id="all" type="number" class="form-control" name="all" value="0" required readonly></td>
						            	</tr>
						            </tfoot>
						        </table>
						        <div class="form-group">
                                    <div class="col-md-12 button-submit">
                                    	<input type="button" class="btn btn-info" name="tambah-item" id="tambah-item" value="Tambah Item">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Bayar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Pembayaran</h4> </div>
                <div class="modal-body" id="printThis">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <table class="table table-bordered responsive" >
					            <thead>
					                <tr>
					                    <th>Nama Menu</th>
					                    <th>Quantity</th>
					                    <th>Harga</th>
					                    <th>Total</th>
					                </tr>
					            </thead>
					            <tbody id="detail_list">

					            </tbody>
					            <tfoot>
					            	<tr>
					            		<td colspan="3" style="vertical-align:middle;text-align: right"><strong>Total Keseluruhan</strong></td>
					            		<td id="detail_all"></td>
					            	</tr>
					            </tfoot>
					        </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
			     	<button type="button" class="btn btn-info" id="print">Print Pembayaran</button>
			     </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_js')
	<script src="{{asset('assets/js/fileinput.js')}}" id="script-resource-8"></script>
    <script type="text/javascript">
    	var counter=1;
    	function set_harga(index,sel){
    		var id = sel.value;
			$.get('menu/find/' + id, function (data) {
		 		console.log(data);
                var harga = data.harga;
				var jumlah = Number($('#jumlah'+index).val());
				var total = harga * jumlah;
				$('#harga'+index).val(data.harga);
	    		$('#total'+index).val(total);
	    		set_all();
            }) 
            
		}
		function set_total(index){
    		var harga = Number($('#harga'+index).val());
			var jumlah = Number($('#jumlah'+index).val());
			var total = harga * jumlah;
    		$('#total'+index).val(total);
    		set_all();
            
		}
		function set_all(){
			var all=0;
    		for(i=0;i<counter;i++)
    		{
    			all += Number($('#total'+i).val())
    		}
            $('#all').val(all);
		}
        $(document).ready(function(){
        	
        	$( "#tambah-item" ).on("click", function() {
		        $( "#baru_list" ).append( 
		        	"<tr><td><select required onchange='set_harga("+counter+",this)' class=\"form-control\" id=\"menu\" name=\"menu["+counter+"]\"><option disabled selected value>-Pilih Menu-</option>@foreach($menu as $menus)<option value=\"{{$menus->id}}\">{{$menus->nama_menu}}</option>@endforeach</select></td><td><input id=\"jumlah"+counter+"\" type=\"number\" class=\"form-control\" name=\"jumlah["+counter+"]\" placeholder=\"Jumlah\" value=\"1\" onchange='set_total("+counter+")' required></td><td><input id=\"harga"+counter+"\" type=\"number\" class=\"form-control\" name=\"harga["+counter+"]\" placeholder=\"Harga\" required readonly></td><td><input id=\"total"+counter+"\" type=\"number\" class=\"form-control\" name=\"total["+counter+"]\" placeholder=\"total\" required readonly \"></td></tr>"
		        	);
		        counter++;
		    });
            var url = "pembayaran/find";
            $('.detail-modal').click(function(){
                var id = $(this).val();
                $.get(url + '/' + id, function (data) {
                    //success data
                    $( "#detail_list" ).html(''); 
                   for(i=0;i<(data.menu).length;i++)
                   {
	                   $( "#detail_list" ).append( 
			        	"<tr><td>"+data.menu[i]+"</td><td>"+data.jumlah[i]+"</td><td>"+data.harga[i]+"</td><td>"+(data.jumlah[i]*data.harga[i])+"</td></tr>"
			        	);
                   }
                   $('#detail_all').html(data.total);
                   $('#detail').modal('show');
                }) 
            });
        });
    </script>

    <script type="text/javascript">
    	document.getElementById("print").onclick = function () {
		    printElement(document.getElementById("printThis"));
		};

		function printElement(elem) {
		    var domClone = elem.cloneNode(true);

		    var $printSection = document.getElementById("printSection");

		    if (!$printSection) {
		        var $printSection = document.createElement("div");
		        $printSection.id = "printSection";
		        document.body.appendChild($printSection);
		    }

		    $printSection.innerHTML = "";
		    $printSection.appendChild(domClone);
		    window.print();
		}
    </script>
@endsection