<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('index');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard')->middleware('auth');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::prefix('/stok')->group(function () {
    Route::get('/', 'StokController@index')->name('stok');
    Route::post('/store', 'StokController@store')->name('stok_baru');
    Route::get('/find/{id}', 'StokController@find')->name('stok_cari');
    Route::post('/edit', 'StokController@edit')->name('stok_edit');
    Route::get('/delete/{id}', 'StokController@delete')->name('stok_hapus');
    Route::post('/tambah', 'StokController@tambah')->name('stok_tambah');
    Route::post('/kurang', 'StokController@kurang')->name('stok_kurang');
});

Route::prefix('/menu')->group(function () {
    Route::get('/', 'MenuController@index')->name('menu');
    Route::post('/store', 'MenuController@store')->name('menu_baru');
    Route::get('/find/{id}', 'MenuController@find')->name('menu_cari');
    Route::post('/edit', 'MenuController@edit')->name('menu_edit');
    Route::get('/delete/{id}', 'MenuController@delete')->name('menu_hapus');
    Route::post('/tambah', 'MenuController@tambah')->name('menu_tambah');
    Route::post('/kurang', 'MenuController@kurang')->name('menu_kurang');
});

Route::prefix('/pembayaran')->group(function () {
    Route::get('/', 'PembayaranController@index')->name('pembayaran');
    Route::post('/store', 'PembayaranController@store')->name('pembayaran_baru');
     Route::get('/find/{id}', 'PembayaranController@find')->name('pembayaran_cari');
     Route::get('/delete/{id}', 'PembayaranController@delete')->name('pembayaran_hapus');
});

Route::prefix('/antrian')->group(function () {
    Route::get('/', 'AntrianController@index')->name('antrian');
    Route::get('/find/{id}', 'AntrianController@find')->name('antrian_cari');
    Route::post('/edit', 'AntrianController@edit')->name('antrian_edit');
    Route::get('/delete/{id}', 'AntrianController@delete')->name('antrian_hapus');
});

Auth::routes();
