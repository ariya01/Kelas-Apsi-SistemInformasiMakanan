<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
     protected $fillable = [
        'menu', 'harga','jumlah','total'
    ];
    protected $casts = [
        'menu' => 'array',
        'harga' => 'array',
        'jumlah' => 'array',
    ];
}
