<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    public function pembayaran()
    {
    	return $this->belongsTo('App\Pembayaran');
    }
    protected $fillable = [
        'pembayaran_id', 'status'
    ];
}
