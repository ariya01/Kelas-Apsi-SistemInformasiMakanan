<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $fillable = [
        'nama_stok', 'jumlah', 'satuan',
    ];
}
