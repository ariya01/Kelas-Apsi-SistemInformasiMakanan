<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Response;
class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public function index()
    {
    	$data=Menu::all();
        return view('menu.show',compact('data'));
    }
    public function store(Request $request)
    {
    	$image=$request->file('image');
    	$data = [
    		'nama_menu' => $request->nama,
    		'kategori' => $request->kategori,
    		'harga' => $request->harga,
    		'jumlah' => $request->jumlah,
    		'deskripsi' => $request->deskripsi,
    		'image' => time().'.'.$image->getClientOriginalExtension(),
    	];

    	$destinationPath = public_path('/images');
    	$image->move($destinationPath, $data['image']);
        $create = Menu::create($data);
        if ($create){
            return redirect(route('menu'))->with('success', trans('Data Menu Baru Berhasil Dibuat'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function find($id)
    {
    	$data = Menu::find($id);
    	return Response::json($data);
    }
    public function edit(Request $request)
    {
    	$data = Menu::find($request->id_edit);
        $data->nama_menu =$request->nama_edit;
        $data->kategori =$request->kategori_edit;
        $data->jumlah =$request->jumlah_edit;
        $data->harga =$request->harga_edit;
        $data->deskripsi =$request->deskripsi_edit;
        if ($data->save()){
            return redirect(route('menu'))->with('success', trans('Data Menu Berhasil Diedit'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function delete($id)
    {
    	$data = Menu::find($id);
    	$data->delete();
        if ($data){
            return redirect(route('menu'))->with('success', trans('Data Menu Berhasil Dihapus'));
        }
        return back()->with('error', trans('Data Menu Gagal Dihapus. Silahkan Coba Lagi'));
    }
    public function tambah(Request $request)
    {
    	$data = Menu::find($request->id_tambah);
        $data->jumlah =$data->jumlah + $request->tambahan;
        if ($data->save()){
            return redirect(route('menu'))->with('success', trans('Data Menu Berhasil Ditambah'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function kurang(Request $request)
    {
    	$data = Menu::find($request->id_kurang);
    	if($data->jumlah<$request->kurangan)return back()->with('error', trans('Kurangan tidak boleh lebih dari jumlah'));
        $data->jumlah =$data->jumlah - $request->kurangan;
        if ($data->save()){
            return redirect(route('menu'))->with('success', trans('Data Menu Berhasil Dikurangi'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
}
