<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stok;
use Response;
class StokController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$data=Stok::all();
        return view('stok.show',compact('data'));
    }
    public function store(Request $request)
    {
    	$data = [
    		'nama_stok' => $request->nama,
    		'jumlah' => $request->jumlah,
    		'satuan' => $request->satuan,
    	];
        $create = Stok::create($data);
        if ($create){
            return redirect(route('stok'))->with('success', trans('Data Stok Baru Berhasil Dibuat'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function find($id)
    {
    	$data = Stok::find($id);
    	return Response::json($data);
    }
    public function edit(Request $request)
    {
    	$data = Stok::find($request->id_edit);
        $data->nama_stok =$request->nama_edit;
        $data->jumlah =$request->jumlah_edit;
        $data->satuan =$request->satuan_edit;
        if ($data->save()){
            return redirect(route('stok'))->with('success', trans('Data Stok Berhasil Diedit'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function delete($id)
    {
    	$data = Stok::find($id);
    	$data->delete();
        if ($data){
            return redirect(route('stok'))->with('success', trans('Data Stok Berhasil Dihapus'));
        }
        return back()->with('error', trans('Data Stok Gagal Dihapus. Silahkan Coba Lagi'));
    }
    public function tambah(Request $request)
    {
    	$data = Stok::find($request->id_tambah);
        $data->jumlah =$data->jumlah + $request->tambahan;
        if ($data->save()){
            return redirect(route('stok'))->with('success', trans('Data Stok Berhasil Ditambah'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function kurang(Request $request)
    {
    	$data = Stok::find($request->id_kurang);
    	if($data->jumlah<$request->kurangan)return back()->with('error', trans('Kurangan tidak boleh lebih dari jumlah'));
        $data->jumlah =$data->jumlah - $request->kurangan;
        if ($data->save()){
            return redirect(route('stok'))->with('success', trans('Data Stok Berhasil Dikurangi'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
}
