<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Pembayaran;
use App\Antrian;
use DB;
use Response;
class PembayaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$menu=Menu::all();
    	$data=Pembayaran::all()->sortByDesc('id') ;
        return view('pembayaran.show',compact('menu','data'));
    }
    public function store(Request $request)
    {
    	$total=0;
    	for($i=0;$i<sizeof($request->menu);$i++)
    	{
    		$temp=Menu::find($request->menu[$i]);
            if($temp->jumlah<$request->jumlah[$i])return back()->with('error', trans('Pembayaran Gagal. Stok Menu Tidak Mencukupi'));
            $temp->jumlah-=$request->jumlah[$i];
    		$menu[$i]=$temp->nama_menu;
    		$total = $total + ($temp->harga*$request->jumlah[$i]);
            $temp->save();
    	}
    	$data = [
    		'menu' => $menu,
    		'harga' => $request->harga,
    		'jumlah' => $request->jumlah,
    		'total' => $request->all,
    		
    	];
        $create = Pembayaran::create($data);
        if ($create){
        	$id = DB::table('pembayarans')->latest()->first();
        	$id=$id->id;
        	$antri = [
	    		'pembayaran_id' => $id,
	    		'status' => 'Proses',
	    	];
	    	$create = Antrian::create($antri);
             return redirect(route('pembayaran'))->with('success', trans('Data Pembayaran Baru Berhasil Dibuat'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function find($id)
    {
    	$data = Pembayaran::find($id);
    	return Response::json($data);
    }
    public function delete($id)
    {
    	$data = Pembayaran::find($id);
    	$data->delete();
        if ($data){
            return redirect(route('pembayaran'))->with('success', trans('Data Pembayaran Berhasil Dihapus'));
        }
        return back()->with('error', trans('Data Pembayaran Gagal Dihapus. Silahkan Coba Lagi'));
    }
}

