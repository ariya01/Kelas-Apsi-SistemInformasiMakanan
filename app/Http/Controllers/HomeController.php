<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stok;
use App\Menu;
use App\Pembayaran;
use App\Antrian;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('dashboard');
    }
    public function dashboard()
    {
        $stok=Stok::count();
        $menu=Menu::count();
        $pembayaran=Pembayaran::count();
        $antrian=Antrian::count();
        return view('dashboard',compact('stok','menu','pembayaran','antrian'));
    }
}
