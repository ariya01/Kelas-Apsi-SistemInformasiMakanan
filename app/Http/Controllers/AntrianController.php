<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Antrian;
use Response;
class AntrianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public function index()
    {
    	$data = Antrian::all()->sortByDesc('id');
        return view('antrian.show',compact('data'));
    }
    public function find($id)
    {
    	$data = Antrian::find($id);
    	return Response::json($data);
    }
    public function edit(Request $request)
    {
    	$data = Antrian::find($request->id_edit);
        $data->status =$request->status;
        if ($data->save()){
            return redirect(route('antrian'))->with('success', trans('Data Antrian Berhasil Diedit'));
        }
        return back()->with('error', trans('Data salah. Silahkan Coba Lagi'));
    }
    public function delete($id)
    {
    	$data = Antrian::find($id);
    	$data->delete();
        if ($data){
            return redirect(route('antrian'))->with('success', trans('Data Antrian Berhasil Dihapus'));
        }
        return back()->with('error', trans('Data Antrian Gagal Dihapus. Silahkan Coba Lagi'));
    }
}
