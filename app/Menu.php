<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'nama_menu', 'kategori','jumlah', 'harga', 'deskripsi','image'
    ];
}
